const Joi = require("joi");
const studentJoischema = Joi.object({
    firstName:Joi.string().required(),
    lastName:Joi.string(),
    email:Joi.string().email().required(),
    postalCode:Joi.number().required(),
    phoneNumber:Joi.number().required().min(10),
    password:Joi.string().min(8),


});
module.exports = {
    studentJoischema
}
// module.exports = mongoose.model("stjoi" , studentJoischema);