const studC = require("../model/studentModel");
const SBMR = require("../model/SubjectMarks")
const bcrypt= require("bcrypt");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const joivali = require("../Validator/studentVali");
const { default: mongoose } = require("mongoose");
const clg = require("../model/college");


//create a new student with new email and hidden password(jwt)
exports.signup = async(req , res)=>{
    try{
        //validaaate using joi
                        //  await User.joiSchema.validate(req.body);
        const {firstName , lastName,  email, postalCode, phoneNumber, password} = req.body;

        const existingUser = await studC.findOne({email});
        if(existingUser){
            return res.status(400).json({
                success:false,
                message:"student already exist"
            })
        }
        //password hashing
        let hashedPassword;
        try{
            hashedPassword = await bcrypt.hash(password , 10);
        }
        catch(error){
            return res.status(500).json({
                success:false,
                message:"password encript unsucessfully",
                error
            })
        }
        //enter user in db
        const newSignUP = await studC.create({
            firstName,lastName,email,postalCode,phoneNumber,password:hashedPassword
        })
        return res.status(200).json({
            success:true,
            message:"Entry created sucessfullyy",
            newSignUP
        })
    }
    catch(error){
        console.error(error);
        return res.status(500).json({
            success:false,
            message:"user cannot register"
        });
    }
}

//retrieving all student from database basically on sorting and paging
module.exports.getAllStudent = async(req , res,next)=>{
    try{
        const page = parseInt(req.query.page)||1;
        const limit  = parseInt(req.query.limit);
        const skip = (page -1)*limit;

        const key = req.query.search || '';
        const allstd = await studC.find({
            "$or":[
                {"firstName":{$regex:key , $options:'i'}},
                {"lastName":{$regex:key , $options:'i'}}
            ]  
        }).limit(limit).skip(skip).sort({firstName:1});
        console.log(allstd);
        const count = await studC.countDocuments({
            "$or":[
                {"firstName":{$regex:key , $options:'i'}},
                {"lastName":{$regex:key , $options:'i'}}
            ]  
        });

        //response
        res.status(200).json({
            success:true,
            data:allstd,
            data2:count,
            message:"Detail of all student"
        })
    }
    catch(error){
        res.status(500).json({
            success:false,
            error:error.message,
            message:"server error"
        })
       
    }
}
//getting one student byt here id and give populated college
module.exports.getOneStudent = async(req , res)=>{
    try{
        const id = req.params.id;
        const student = await studC.findById({_id:id}).populate("collegeName").exec();
        if(!student){
            res.status(404).json({
                sucess:false,
                message:"no student find with such id"
            })
        }
        res.status(200).json({
            success:true,
            data:student,
            message:"student detail is done with populated college"
        })

    }
    catch(error){
        return res.status(500).json({
            success:false,
            message:"server error"
        })
    }
}
//update one student
module.exports.updatedUser = async(req , res)=>{
    try{
        const id = req.params.id;
        const { firstName , lastName ,email , postalCode,phoneNumber} = req.body;
        const updateOneS = await studC.findByIdAndUpdate({_id:id} , {firstName  , lastName ,email ,postalCode ,phoneNumber} , {new:true});
        if(!updateOneS){
            //throw new Error("hey")
            return res.status(404).json({
                success:false,
                message:"no data  found"
            })
        }
        //response

        res.status(200).json({
            success:true,
            data:updateOneS,
            message:"Value of one student is updated"
        })
    }
    catch(error){
        res.status(500).json({
            success:false,
            error:error.message,
            message:"server error"
        })
       
    }
}
//delete one student
module.exports.deleteOneStudent = async(req , res)=>{
    try{
        const id = req.params.id; //const {id} = req,params
        const dlt = await studC.findByIdAndDelete({_id:id});
        //response
        res.status(200).json({
            success:true,
            data:dlt,
            message:"Detail of all student"
        })
    }
    catch(error){
        res.status(500).json({
            success:false,
            error:error.message,
            message:"server error"
        })
       
    }
}
//finding oen marks by their _id 
//nested population     MarksID=>populate student =>then populate college
module.exports.getOneMArks = async(req , res)=>{
    try{
        const id = req.params.id;
        const oneMark = await SBMR.findById({_id:id}).populate({path:"student", populate:{path:"collegeName"}}).exec();
        if(!oneMark){
            return res.status(404).json({
                success:false,
                message:"no data  found"
            })
        }
        //response

        return res.status(200).json({
            success:true,
            data:oneMark,
            message:"Detail of one marks"
        })
    }
    catch(error){
        return res.status(500).json({
            success:false,
            message:"server error"
        })
       
    }
}


//createing subject-marks and student  object using 
module.exports.updateMarks = async(req , res)=>{
    try{
        //fetching data from req body
        const {student, subject, Marks} = req.body;
        //create a subject-marks object
        const subjectMarks2 = new SBMR({student,subject,Marks});
        //save object in database
        const  subjectMarksObj = await subjectMarks2.save();
        const oneStud = await studC.findByIdAndUpdate( student, {$push:{subjectMarks:subjectMarksObj._id}}, {new:true})
                                        .populate('subjectMarks').exec()

        res.status(200).json({
            success:true,
            data:subjectMarksObj,
            data2:oneStud,
            message:"subject Marks of all student"
        })
    }
    catch(error){
        res.status(500).json({
            success:false,
            error:error.message,
            message:"server error"
        })
       
    }
}

//getting all marks of one student using student_id
exports.studentMarks = async(req ,res)=>{
    try{
        const id = req.params.id;
        console.log(id);
        const Marks = await SBMR.find({ student: id });
        console.log(Marks);
        if(!Marks){
            return res.status(400).json({
                sucess:false,
                message:"No such data found"
            })
        }
        return res.status(200).json({
            sucess:true,
            message:"entry created sucessfully",
            Marks
        })

    }
    catch(error){
        res.status(500).json({
            success:false,
            error:error.message,
            message:"server error"
        })
    }
}
//updateMarks
exports.updateStudentMarks = async(req,res)=>{
    try{
        const id = req.params.id;
        const { subject , Marks} = req.body;
        if(Marks==0){
            return res.status(404).json({
                message:"Marks cannot be 0",
            })
        }
        const updatedMarks1 = await SBMR.findByIdAndUpdate({_id:id}, {subject,Marks},{new:true});
        if(!updatedMarks1){
            return res.status(400).json({
                sucess:false,
                message:"No student find from that id",
                updatedMarks1
            })
        }
        return res.status(200).json({
            sucess:true,
            message:"marks updates sucessfully",
            updatedMarks1

        })
    }
    catch(error){
        console.error(error);
        return res.status(500).json({
            success:false,
            message:"Marks cannot update",
        });
    }
}
//delete marks of 1 subject
exports.deleteSubjectMark = async(req,res)=>{
    try{
        const id = req.params.id;
        const deletedMark = await SBMR.findByIdAndDelete({_id:id});
        if(!deletedMark){
            res.ststus(400).json({
                sucess:false,
                message:"No such marks find",
                deletedMark
            })
        }
        res.status(200).json({
            sucess:true,
            message:"marks deleted Sucessfully",
            deletedMark
        })

    }catch(error){
        console.error(error);
        return res.status(500).json({
            success:false,
            message:"Marks cannot deleted",
        });
    }
}
//lognin with password
exports.LoginP = async(req , res)=>{
    try{
        //data fetch
        const {email , password , phoneNumber} = req.body;
        //validate email and password
        if( !password || (!phoneNumber && !email)){
            return res.status(400).json({
                success:false,
                message:"please fill the detail carefully"
            });
        }
        //user is in db?
        const student1 = await studC.findOne({email});
        if(!student1){
            return res.status(401).json({
                success:false,
                message:"student is not registeres , so sign up first"
            })
        }
        const payload={
            email:student1.email,
            id:student1._id,
        }
        //varify password and generate jwt token
        if(await bcrypt.compare(password, student1.password) && (student1.email== email || student1.phoneNumber == phoneNumber)){
            //yes password match
            let token = jwt.sign(payload, process.env.JWT_SECRET, {expiresIn:"24h"});
            student1.token = token;
            return res.status(200).json({
                sucess:true,
                message:"bhai same emailid, password and portalcode  and phone Numbermatch ho gya hai",
                token
            })
        }
        else{
            //password donot match
            return res.status(403).json({
                success:false,
                message:"values inCorrect"
            })
        }
    }
    catch(error){
        console.error(error);
        return res.status(500).json({
            success:false,
            message:"user cannot Login Please try after some Time"
        });

    }
}
//getting student using token
exports.getinfo = async(req, res)=>{
    require("dotenv").config();
    try{
        const token2= req.body.token;
        if(!token2){
            return res.status(401)({
                sucess:false,
                message:" please give me token"
            });
        }
        const var1 = await jwt.verify(token2, process.env.JWT_SECRET);
        const var2 = await studC.findOne({_id:var1.id});
        console.log(var1);
        return res.status(200).json({
            success:true,
            message:"studentFindSucessfully",
            var2

        })
    }
    catch(error){
        console.error(error);
        return res.status(500).json({
            success:false,
            message:"cannot find student from the given token",
        });

    }
}

//node Mailer
exports.getmail = async(req , res)=>{
    try{
        const email = req.body.email
        console.log(email);
        const transporter = nodemailer.createTransport({
            host:"smtp.gmail.com",
            port:465,
            secure:true,
            auth:{
                user:"au599707@gmail.com",
                pass:"yvsz xpzj mgrz kqch",
            }
        });
        const info = await transporter.sendMail({
            from: '"Aman"<au599707@gmail.com>',
            to: email,
            subject: "This is related to nodeAPI", 
            text: "Hello Aman we are sending this email for deo check of email nodemailer",
            html: "<b>Hiiii</b>",
        });
        transporter.close();
        return res.status(200).json({
            status:true,
            message:"Emailsend sucessfully",
            info
        })
    }
    catch(error){
        console.error(error);
        return res.status(500).json({
            success:false,
            message:"cannot send email",
        });
    }
}
//sending mail id subject marks is greater than 5
exports.sendMial5 = async(req,res)=>{
    try{
        const id = req.params.id;
        const student5 = await studC.findById({_id:id})
        const email = student5.email;
        console.log(email);
        const transporter = nodemailer.createTransport({
            host:"smtp.gmail.com",
            port:465,
            secure:true,
            auth:{
                user:"au599707@gmail.com",
                pass:"yvsz xpzj mgrz kqch",
            }
        });
        const info = await transporter.sendMail({
            from: '"Aman"<au599707@gmail.com>',
            to: email,
            subject: "subject marks Update", 
            text: "Hello student we are sending you this email , beacuse your subject marks has been updated",
            html: "<b>Hiiii</b>",
            replyTo:"fullstacktester@yopmail.com"
        });
        transporter.close();
        return res.status(200).json({
            status:true,
            message:"Emailsend sucessfully",
            info
        })

    }
    catch(error){
        console.error(error);
        return res.status(500).json({
            success:false,
            message:"cannot send email",
        });
    }
}

//finnding api
exports.search = async(req,res)=>{
    try{
      const key = req.params.key;
      const newObj = await studC.find({
        "$or":[
            {"firstName":{$regex:key , $options:'i'}},
            {"lastName":{$regex:key , $options:'i'}}
        ]
      })
      return res.status(200).json({
        sucess:"true",
        message:"seach sucesssfully",
        newObj
      })

    }
    catch(error){
        console.error(error);
        return res.status(500).json({
            success:false,
            message:"Seach cannot happen",
        });
    }
}

//Inserting college name ad pushing its _id in college of student schema
exports.college = async(req,res)=>{
    try{
        const {collegeName, student} = req.body;
        //create a subject-marks object
        const college1 = new clg({student,collegeName});
        //save object in database
        const  college2 = await college1.save();
        const oneStud1 = await studC.findByIdAndUpdate( student, {$push:{collegeName:college2._id}}, {new:true})
                                        .populate('collegeName').exec();
        console.log(oneStud1.collegeName);

        res.status(200).json({
            success:true,
            data:college2,
            data2:oneStud1,
            message:"subject Marks of all student"
        })
    }
    catch(error){
        console.error(error);
        return res.status(500).json({
            success:false,
            message:"populate cannot happen",
        });
    }
}
exports.projectionapi = async(req, res)=>{
    try{
        const student =  await studC.find({},{firstName:1 , lastName:1});
        return res.status(200).json({
            sucess:true,
            student,
        })

    }catch(error){
        console.error(error);
        return res.status(500).json({
            success:false,
            message:"projeected api cannot happen",
        });
    }

}
//aggreGTING api lookup , unwind ,projection ,sort
exports.agg = async(req ,res)=>{
    try{
        const marks = await SBMR.aggregate([
            {
                $lookup: {
                  from: "stdcs",
                  localField: "student",
                  foreignField: "_id",
                  as: "myresult"
                }
            },
            {
                $unwind: "$myresult"

            },
            {
                $project:{
                    "subject":1,
                    "Marks":1,
                    "myresult.firstName":1,
                    "myresult.lastName":1,
                }
            },
            {
                $limit:7
            },{
                $sort:{"subject":1}
            }
        ])  
        return res.status(200).json({
            sucess:true,
            marks,
        }) 
    }
    catch(error){
         res.status(500).json({
            success:false,
            message:"aggregate cannot happen",
        });
    }
}


