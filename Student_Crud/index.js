const express = require("express");
const app = express();

const cors  = require("cors");

//listening ons erver
require("dotenv").config();
const PORT = process.env.PORT || 3005;

//body Parser
app.use(express.json());
app.use(cors({origin:" http://localhost:4200"}));

//route and route mounting
const crudop = require("./router/crud")
app.use("/api/v1" , crudop);

app.listen(PORT ,()=>{
    console.log(`server is running on port${PORT}`);
    const dbconnect = require("./config/database");
    dbconnect();
})
app.get("/" , (req , res)=>{
    res.send("setup is done");
})

