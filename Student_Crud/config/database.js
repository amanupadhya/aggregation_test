const mongoose  = require("mongoose");
require("dotenv").config();

const connectivityWithDb2 = ()=>{
    mongoose.connect(process.env.DATABASE_URL ,{
        useNewUrlParser  : true,
        useUnifiedTopology : true
    })
    .then( ()=>{
        console.log("database connected successfully")
    })
    .catch( (error)=>{
        console.log("Db facing some issue  while connecting in database");
        console.log(error);
        process.exit(1);
    })
}
module.exports = connectivityWithDb2;