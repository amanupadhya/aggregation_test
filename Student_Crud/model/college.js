const mongoose = require('mongoose');
const collSchema = new mongoose.Schema({
    collegeName:{
        type:String,
        required:true,
    },
    student:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"stdC"
    }
    // students:[{
    //     type:mongoose.Schema.Types.ObjectId,
    //     ref:"stdc",
    // }],
    // marks:[{
    //     type:mongoose.Schema.Types.ObjectId,
    //     ref:"SBMR"
    // }]
})
module.exports = mongoose.model("clg" , collSchema);