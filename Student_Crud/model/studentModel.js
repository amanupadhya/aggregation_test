const mongoose = require("mongoose");
const studentSchema = new mongoose.Schema({
    firstName:{
        type:String,
        required:true,
        maxLength:20
    },
    lastName:{
        type:String,
        required:true,
        maxLength :20
    },
    email:{
        type: String,
        required : true
    },
    postalCode:{
        type:Number,
        required:true,
        maxLength:2,
    },
    phoneNumber:{
        type:Number,
        required : true,
        uique:true,
        maxLength:10,
    
    },
    subjectMarks:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:"SBMR"
    }],
    password:{
        type:String,
    },
    collegeName:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"clg",default:null
    }

});
module.exports = mongoose.model("stdC" , studentSchema);