const mongoose = require("mongoose");
const subjectMarksSchema = new mongoose.Schema({
    // English:{
    //     type:Number
    // },
    // Hindi:{
    //     type:Number
    // },
    // Maths:{
    //     type:Number
    // },
    // physics:{
    //     type:Number
    // }
    student:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"stdC"
    },
    subject:{
        type:String
    },
    Marks:{
        type:Number
    }
    // college:{
    //     type.mon
    // }
})
module.exports = mongoose.model("SBMR" , subjectMarksSchema);