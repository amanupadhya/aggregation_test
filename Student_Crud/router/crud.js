const express = require('express');
const route = express.Router();

// import controller
const controller = require("../controller/studentdetail");

//define route
route.get('/getAllStudent' ,controller.getAllStudent)  //
route.get('/oneStudent/:id' , controller.getOneStudent)  //here populate college
route.put('/updateStudent/:id' , controller.updatedUser)
route.delete('/deleteStudent/:id' , controller.deleteOneStudent)

//marks api
route.post('/updateSubjectMarks/', controller.updateMarks)
route.get('/SubjectMarks/:id' , controller.studentMarks) //getting allmarks of student_id
route.delete('/deleteMarks/:id',controller.deleteSubjectMark)
route.put('/updateStudentMarks/:id' ,controller.updateStudentMarks);

//sign up
route.post('/studentsignup' , controller.signup);
//login with email , password
route.post('/studentloginPassword' , controller.LoginP)
//getting information of student using token
route.post('/getinformation', controller.getinfo)

//find search api
route.get('/search/:key' , controller.search);

//Mailer
route.post('/sendmail' , controller.getmail);
//Multer
const multer= require("multer");
const upload = multer({
    storage:multer.diskStorage({
        destination:function(req,file,cb){
            cb(null,'image')
        },
        filename:function (req,file,cb){
            cb(null,file.fieldname + "-"+ Date.now() + ".jpg")
        }
    })
}).single("user_file")
route.post('/Uploadfile' ,upload,  (req, res)=>{
    res.send("fileUploaded")
});


//mail if subject is more than 5
route.post('/subject5/:id' ,controller.sendMial5);



//collge schem routes
route.post('/college',controller.college)

//projection api only for username
route.get("/getentyusingprojection" , controller.projectionapi)
//getting one marks and populate student
route.get("/getonemarks/:id", controller.getOneMArks)  /

//aggreagation
route.get("/aggerigation",controller.agg);

//export route
module.exports = route;